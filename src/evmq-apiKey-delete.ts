import { AxiosResponse } from 'axios';
import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { ApiKeyListResponse } from 'evmq-sdk';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('delete')
  .option('-i --id <id>','api key id.')
  .option('-l --localOnly ','only delete local api-key, leaving remote api-key active.')

commandWrapper(program, async ({evmQueueSdk, output, apiKeys, credentials})=>{
  const { id, localOnly } = program.opts();
  if(!id) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-i --id"')
  let res: AxiosResponse<ApiKeyListResponse & {error: string}>
  await credentials.hydrateFromDisk();
  evmQueueSdk.setIdToken({ idToken: credentials.idToken });
  if (!localOnly) {
    res = await evmQueueSdk.apiKeyDelete({id});
  }
  const localDelete = await apiKeys.delete({id});
  output.send({ localDelete, remoteDelete: res?.status === 200});
})