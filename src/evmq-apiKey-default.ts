import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('default')
  .option('-i --id <id>', "Api Key Id")

commandWrapper(program, async ({ output, apiKeys })=>{
  const { id } = program.opts();
  await apiKeys.hydrateFromDisk();
  if(!id) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-i --id"')
  await apiKeys.setDefault({ id })
  await apiKeys.save();
  output.send(apiKeys.asOutput());
})