import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('register')
  .option('-e, --email [email]', 'Email address. A email will be sent to verify.')
  .option('-p, --password [password]', 'Password. You will use this password to login later.')

commandWrapper(program, async ({ evmQueueSdk, output }) => {
  const { email, password } = program.opts()
  if(!email) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --email"')
  if(!password) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-p, --password"')
  const res = await evmQueueSdk.register({ email, password });
  if(res.data.error) {
    output.send(res.data);
  } else {
    output.send({
      confirmed: res.data.data.UserConfirmed, 
      confirmationSent: res.data.data.CodeDeliveryDetails.Destination
    })
  }
})