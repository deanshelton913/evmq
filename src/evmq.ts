#!/usr/bin/env node
import { Command } from 'commander';
import { version } from '../package.json';
const program = new Command();

program
  .name('evmq')
  .version(version)
  .command('account', 'Create and manage your Account')
  .command('apiKey', 'Create and manage your Api-Keys')
  .command('job', 'Create and manage your Jobs')

program.parse();