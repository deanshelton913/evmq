import { Command } from 'commander';

const program = new Command();
program
  .name('job')
  .command('create', 'Create a new job')
  .command('list', 'List all jobs by id')
  .command('show', 'Show details of a single job')
  .command('cancel', 'Cancel job by id')
  .command('read', 'Pop <n> items from the queue for this job')
  program.parse();
