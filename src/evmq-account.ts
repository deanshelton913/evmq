import { Command } from 'commander';

const program = new Command();

program
  .name('account')
  .command('login', 'login to the evmqueue app. storing your id and access tokens to disk.')
  .command('logout', 'logout globally. This will log you out of all devices.')
  .command('register', 'register a new account.')
  .command('verify', 'verify your email using the code send to your inbox.')
  .command('verify-resend', 'resend verification code.')
  .command('forgot-password', 'send email to recover your lost password.')
  .command('forgot-password-verify', 'verify your new password');
  program.parse();
