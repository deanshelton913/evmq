import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();

program
  .name('logout')

commandWrapper(program, async ({ output, credentials, evmQueueSdk })=>{
  await credentials.hydrateFromDisk();
  evmQueueSdk.setAccessToken({ accessToken: credentials.accessToken });
  const res = await evmQueueSdk.logout();
  if (res.status === 400) {
    throw new FailureByDesign('BAD_REQUEST', res.data?.error)
  }
  await credentials.destroy();
  
  output.send({ loggedOut: true });
})