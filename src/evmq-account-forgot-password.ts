import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('forgot-password')
  .option('-e, --email [email]', 'Email address. A email will be sent to verify.')

commandWrapper(program, async ({ evmQueueSdk, output })=>{
  const { email, confirmationCode } = program.opts()
  if(!email) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --email"')
  const res = await evmQueueSdk.forgotPassword({email})
  output.send(res.data);
})