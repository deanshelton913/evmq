import Ajv from "ajv"
import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import prompts from 'prompts';
import { FailureByDesign } from './lib/FailureByDesign';

const ajv = new Ajv()
const program = new Command();

program
  .name('create')
  .option('-y --skip-prompts','Skip prompts, and use params for direct input')
  .option('-s --start-block <blockNumber>','Starting block (inclusive) for this job')
  .option('-e --end-block <blockNumber>','Ending block (inclusive) for this job (-1=run forever)')
  .option('-f --event-filter <filter>','EventFilter as a JSON string (see documentation)')
  .option('-n --max-scale-limit <limit>','number of parallel workers. 1=FIFO. 2-20=parallel.')
  .option('-r --rpc-url <urlOrSlug>','url of an RPC endpoint or a network slug e.g.: EVMQUEUE_ETHEREUM_MAINNET.')
  .option('-l --spending-limit <limit>','monthly spending limit for this job in USD. 5-200')



  export const eventFilterSchema = {
    title: 'eventFilterSchema',
    type: 'object',
    additionalProperties: false,
    properties: {
      address: { oneOf:[
        { type: "object", nullable: true },
        { type: "string"}
      ]},
      topics: {
        type: 'array',
        minItems: 1,
        items: {
          oneOf:[
            { type: "object", nullable: true },
            { 
            type: 'array',
            minItems: 1,
            uniqueItems: true,
            items: { 
              oneOf:[
                { type: "object", nullable: true },
                { type: "string"}
              ]
            }
          }]
        }
      },
    },
    anyOf: [{required: ["address"]},{required: ["topics"]}]
  };
  const eventFilterSchemaValidator = ajv.compile(eventFilterSchema);
  
const mem = { 
  network: null, 
  topics:[null, null, null], 
  address: null, 
  spendingLimit: null, 
  maxScaleLimit: null,
  startBlock: null,
  endBlock: null
};

const ourNodes = {
  "Eth Mainnet (evmqueue.com)": 'EVMQUEUE_ETHEREUM_MAINNET',
  "Eth Testnet[Ropsten] (evmqueue.com)": 'EVMQUEUE_ROPSTEN_TESTNET',
}

const ourTopics = {
  ERC_20_TRANSFER: '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
  ERC_721_TRANSFER: '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
  ERC_1155_TRANSFER: '0xc3d58168c5ae7397731d063d5bbf3d657854427343f4c083240f7aacaa2d0f62',
  ERC_1155_TRANSFER_BATCH: '0x4a39dc06d4c0dbc64b70af90fd698a233a518aa5d07e595d983b8c0526c8f7fb',
}

const networkPrompt = {
  type: 'select',
  name: 'value',
  message: 'Select an RPC node for your job.',
  choices: [
    ...Object.keys(ourNodes).map(x=>({title:x, value:ourNodes[x]})),
    { title: 'I want to use my own RPC node', value: 'CUSTOM' },
  ],
}
const eventTopicPrompt = {
  type: 'select',
  name: 'value',
  message: 'What eth_getLogs topic[0] do you want to watch?',
  choices: [
    ...Object.keys(ourTopics).map(x=>({title:x, value:x})),
    { title: 'ALL_EVENTS', value: 'ALL_EVENTS' },
    { title: 'CUSTOM', value: 'CUSTOM' },
  ],
}

const customTopic0Prompt = {
  type: 'text',
  name: 'value',
  message: 'What method signature(s) are you interested in?',
  validate: value => !/^0x[a-fA-F0-9]*$/.test(value) ? `Topic must be valid Hex. (ex: 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef)` : true

}

const customTopic1Toggle = {
  type: 'toggle',
  name: 'value',
  message: 'Do you want to further filter by a hex encoded argument at position 0?',
  initial: false,
  active: 'yes',
  inactive: 'no'
}

const customTopic1Prompt = {
  type: 'text',
  name: 'value',
  message: 'What is the indexed argument at position 0?',
  validate: value => !/^0x[a-fA-F0-9]*$/.test(value) ? `Topic must be valid Hex. (ex: 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef)` : true
}

const customTopic2Toggle = {
  type: 'toggle',
  name: 'value',
  message: 'Do you want to further filter by a hex encoded argument at position 1?',
  initial: false,
  active: 'yes',
  inactive: 'no'
}

const customTopic2Prompt = {
  type: 'text',
  name: 'value',
  message: 'What is the indexed argument at position 1?',
  validate: value => !/^0x[a-fA-F0-9]*$/.test(value) ? `Topic must be valid Hex. (ex: 0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef)` : true
}

const customNetworkPrompt = {
  type: 'text',
  name: 'value',
  message: `What's the URL of your RPC interface?`,
  validate: value => !/^(http|https):\/\/.*?$/.test(value) ? `URL must be "http" or "https".` : true
}

const addressPrompt = {
  type: 'text',
  name: 'value',
  message: `What address would you like to watch? (leave blank for "all addresses")`,
  validate: value => !/^(0x[a-fA-F0-9]{40}|)$/.test(value) ? `Must be a valid Eth Address (ex: 0x0000000000000000000000000000000000000000).` : true
}

const setSpendingLimitPrompt = {
  type: 'number',
  name: 'value',
  message: 'Monthly spending limit for this job in USD.',
  increment: 5,
  initial: 20,
  min: 5,
  max: 200
}

const setMaxScalePrompt = {
  type: 'number',
  name: 'value',
  message: 'Max number of workers. 1=FIFO. 2-20=parallel',
  initial: 1,
  min: 1,
  max: 20,
  validate: value => value > 20 ? `For more than 20 workers please contact support` : true
}

const startBlockPrompt = {
  type: 'number',
  name: 'value',
  message: 'What is the startBlock of this job (inclusive)?',
  initial: 1,
  min: 1,
}

const runForeverPrompt = {
  type: 'toggle',
  name: 'value',
  message: 'How should this job end?',
  initial: false,
  active: 'Run until I cancel',
  inactive: 'Stop at a specific block number'
}

const endBlockPrompt = {
  type: 'number',
  name: 'value',
  message: 'What is the endBlock of this job (inclusive)?',
  initial: mem.startBlock,
  min: mem.startBlock,
  validate: val => val < mem.startBlock ? `endBlock must be >= startBlock (${mem.startBlock})`: true
}


const areYouSure = {
  type: 'toggle',
  name: 'value',
  message: 'Create this job?',
  initial: false,
  active: 'yes',
  inactive: 'no'
}

function validateEventFilter(eventFilterString:string){
  let filter: string;
  try {
    filter = JSON.parse(eventFilterString)
  } catch {
    throw new FailureByDesign('PARAM_ERROR','event filter is not valid JSON')
  }
  const valid = eventFilterSchemaValidator(filter)
  if(!valid) throw new FailureByDesign('PARAM_ERROR', JSON.stringify(eventFilterSchemaValidator.errors));
  return eventFilterString
}

commandWrapper(program, async ({evmQueueSdk, output, apiKeys})=>{
  const { skipPrompts, startBlock, endBlock, eventFilter, maxScaleLimit, spendingLimit, rpcUrl } = program.opts();

  await apiKeys.hydrateFromDisk();
  evmQueueSdk.setApiKey({ apiKey: apiKeys.getDefault().key });
  if(skipPrompts) {
    if(!startBlock) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-s --start-block"')
    if(!endBlock) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --end-block"')
    if(!eventFilter) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-f --event-filter"')
    if(!maxScaleLimit) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-n --scale-limit"')
    if(!rpcUrl) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-r --rpc-url"')
    if(!spendingLimit) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-l --spending-limit"')
    

    const result = await evmQueueSdk.jobCreate({
      startBlock: Number(startBlock),
      endBlock: Number(endBlock),
      eventFilter: validateEventFilter(eventFilter),
      maxScaleLimit: Number(maxScaleLimit),
      rpcUrl,
      spendingLimit: Number(spendingLimit),
      chainHeadBuffer: 1,
      maxLogSpanInBlocks: 100,
      maxWorkerBlockSpan: 100
    });
    output.send(result.data.data)

  } else {

    // NETWORK:
    const network = await prompts(networkPrompt);
    if(!network.value) return 
    if(network.value === 'CUSTOM') {
      const response = await prompts(customNetworkPrompt);
      mem.network = response.value;
    } else {
      console.log(network.value, ourNodes)
      mem.network = network.value
    }

    // TOPICS
    const eventType = await prompts(eventTopicPrompt);
    if(eventType.value === 'ALL_EVENTS') {
      mem.topics = null
    } else {
      if(eventType.value !== 'CUSTOM') {
        mem.topics[0] = [ourTopics[eventType.value as keyof typeof ourTopics]]
      } else {
        const response0 = await prompts(customTopic0Prompt);
        mem.topics[0] = [response0.value]
        const wantToAddTopic1 = await prompts(customTopic1Toggle);
        if(wantToAddTopic1.value) {
          const topic1 = await prompts(customTopic1Prompt);
          mem.topics[1] = [topic1.value];
        } else {
          mem.topics[1] = null;
        }
        const wantToAddTopic2 = await prompts(customTopic2Toggle);
        if(wantToAddTopic2.value){
          const topic2 = await prompts(customTopic2Prompt);
          mem.topics[2] = [topic2.value];
        } else {
          mem.topics[2] = null;
        }
      }
    }

    // ADDRESS
    const address = await prompts(addressPrompt);
    mem.address = address.value || null
    const spendingLimit = await prompts(setSpendingLimitPrompt);
    mem.spendingLimit = spendingLimit.value
    const maxScaleLimit = await prompts(setMaxScalePrompt);
    mem.maxScaleLimit = maxScaleLimit.value
    const startBlock = await prompts(startBlockPrompt);
    mem.startBlock = startBlock.value
    const runForever = await prompts(runForeverPrompt);
    if(runForever.value) {
      mem.endBlock = -1
    } else {
      const endBlock = await prompts(endBlockPrompt);
      mem.endBlock = endBlock.value
    }
    const payload = {
      startBlock: mem.startBlock,
      endBlock: mem.endBlock,
      eventFilter: validateEventFilter(JSON.stringify({ topics: mem.topics, address: mem.address})),
      maxScaleLimit: mem.maxScaleLimit,
      rpcUrl: mem.network,
      spendingLimit: mem.spendingLimit,
      chainHeadBuffer: 1,
      maxLogSpanInBlocks: 100,
      maxWorkerBlockSpan: 5000
    }
    console.log(JSON.stringify(payload, null, 2))
    const iAmSure = await prompts(areYouSure);
    if(iAmSure.value) {
      const result = await evmQueueSdk.jobCreate(payload);
      output.send(result.data.data)
    } else {
      console.log('CANCELLED')
    }
  }
});

