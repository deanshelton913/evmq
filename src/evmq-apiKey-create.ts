import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('create');

commandWrapper(program, async ({evmQueueSdk, output, apiKeys, credentials})=>{
  await credentials.hydrateFromDisk();
  evmQueueSdk.setIdToken({ idToken: credentials.idToken });
  const res = await evmQueueSdk.apiKeyCreate();
  apiKeys.hydrateFromApiResponse({apiResponse: res.data});
  await apiKeys.save();
  output.send(res.data.data);
})