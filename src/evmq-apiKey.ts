import { Command } from 'commander';

const program = new Command();
program
  .name('apiKey')
  .command('create', 'create a new job')
  .command('list', 'list all api-keys by id')
  .command('default', 'set an existing local key to default')
  .command('add', 'add a previously created api-key to disk for use with this CLI')
  .command('delete', 'delete api-key by id')
  program.parse();
