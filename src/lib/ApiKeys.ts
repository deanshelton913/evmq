import os from 'os'
import fs from 'fs'
import path from 'path'
import { RootLogger } from 'loglevel';
import { FailureByDesign } from './FailureByDesign';
import { ApiKeyCreateResponse } from 'evmq-sdk'



interface SingleKey { key: string, deleted?: boolean };
interface Keys { [id: string]: SingleKey };
interface ApiKeysAtRest {
  defaultKeyId: string;
  keys: Keys
}
export class ApiKeys {
  public static DIRECTORY = path.join(os.homedir(),'.evmqueue');
  public static FILEPATH =  path.join(ApiKeys.DIRECTORY,'api-keys.json');
  public logger: RootLogger;
  public keys: Keys
  public defaultKeyId: string

  constructor({ 
    logger, 
    keys,
    defaultKeyId
  } :{ 
    logger: RootLogger, 
    keys?: Keys,
    defaultKeyId?: string
  }){
    this.logger = logger;
    this.keys = keys;
    this.defaultKeyId = defaultKeyId;
  }

  public asOutput() {
    return Object.keys(this.keys)
      .filter(id=>!this.keys[id].deleted)
      .map(id=>({id, key: this.keys[id].key, isDefault: id === this.defaultKeyId}))
  }

  public async hydrateFromDisk() {
    const res = await this._readFromDisk();
    this.keys = res.keys;
    this.defaultKeyId = res.defaultKeyId;
  }

  public getDefault() {
    return this.keys[this.defaultKeyId]
  }

  public hydrateFromApiResponse({ apiResponse }:{ apiResponse: ApiKeyCreateResponse }) {
    this.defaultKeyId = apiResponse?.data?.apiKeyId;
    this.keys = {
      [apiResponse.data.apiKeyId]:{
        key: apiResponse.data.apiKey,
      }
    }
  }
  
  public async delete ({id}:{id:string}){
    await this.hydrateFromDisk();
    if(this.keys[id] !== undefined && !this.keys[id].deleted) {
      this.keys[id].deleted = true;
      if(this.defaultKeyId === id) this.defaultKeyId = null;
      await this.save();
      return true
    }
    return false
  }

  public add ({id, key}:{id:string, key: string}) {
    this.defaultKeyId = id;
    this.keys = { [id]: { key }}
  }

  public async setDefault ({id}:{id:string}) {
    await this.hydrateFromDisk()
    if (this.keys[id] === undefined) throw new FailureByDesign('PARAM_ERROR',`Key ID ${id} does not exist locally.`)
    this.defaultKeyId = id;
  }

  public async save() {
    await this._createDirectoryIfNeeded();
    const exists = await fs.existsSync(ApiKeys.FILEPATH);
    let allKeysAndDefault = this.toJson();
    if(exists) {
      const keysFromDisk = await this._readFromDisk();
      allKeysAndDefault.keys = {...keysFromDisk.keys, ...allKeysAndDefault.keys};
    }
    this.keys = allKeysAndDefault.keys
    this.logger.info(`Writing to ${ApiKeys.FILEPATH}`);
    await fs.promises.writeFile(ApiKeys.FILEPATH, JSON.stringify(allKeysAndDefault, null, 2));
    this.logger.info(`Write complete`);
  }

  public toJson(){
    return {
      defaultKeyId: this.defaultKeyId,
      keys: this.keys
    } as ApiKeysAtRest
  }

  private async _createDirectoryIfNeeded() {
    const exists = await fs.existsSync(ApiKeys.DIRECTORY);
    if(!exists){
      await fs.promises.mkdir(ApiKeys.DIRECTORY);
    }
  }


  private async _readFromDisk() {
    this.logger.info(`Reading from ${ApiKeys.FILEPATH}`);
    const exists = await fs.existsSync(ApiKeys.FILEPATH);
    if(!exists) throw new FailureByDesign('UNAUTHENTICATED','Local apiKey file not found. If you already created keys, add them with the "add" command.');
    const str = await fs.promises.readFile(ApiKeys.FILEPATH,'utf-8');
    this.logger.info(`Read complete`);
    return JSON.parse(str) as ApiKeysAtRest;
  }

}


