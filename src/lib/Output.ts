import { FailureByDesign } from "./FailureByDesign";

type ValidStyle = 'json'|'table';

export class Output {
  public static VALID = ['json','table']
  public format: ValidStyle;

  constructor(options: { format?: string }={}) {
    if (options.format) {
      if (!Output.VALID.includes(options.format)) {
        throw new FailureByDesign('PARAM_ERROR',`"-o --output" must be one of ${Output.VALID.join(',')}`)
      }
    }
    this.format = options.format as ValidStyle || 'table';
  }
  public send(msg: any) {
    console.log(JSON.stringify(msg, null, 2))
    // if(this.format === 'json') {
    // } else {
    //   if(Array.isArray(msg)){
    //     let table = new Table({ head: Object.keys(msg[0]) });
    //     msg.forEach(x=>table.push(Object.values(x) as any))
    //     console.log(table.toString());
    //   }else{
    //     let table = new Table({ head: [] });
    //     Object.keys(msg).map(k=>({[k]:msg[k]})).forEach(x=>table.push(x))
    //     console.log(table.toString());
    //   }
    // }
  }
}