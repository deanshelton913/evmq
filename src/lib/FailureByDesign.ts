type ErrorKind =
  | 'PARAM_ERROR'
  | 'SUBSYSTEM_ERROR'
  | 'BAD_REQUEST'
  | 'UNAUTHENTICATED'
  | 'FORBIDDEN'
  | 'UNEXPECTED'
  
export class FailureByDesign extends Error {
  public kind: ErrorKind;
  public diagnosticInfo: any

  /**
   * @param {ErrorKind} kind The kind of error which changes how this error should be handled.
   * @param {any} message The error message.
   */
  constructor(kind: ErrorKind, message: any, diagnosticInfo?: any ) {
    super(message);
    this.message = message;
    this.diagnosticInfo = diagnosticInfo;
    this.kind = kind;
    this.name = 'FailureByDesign';
  }
}