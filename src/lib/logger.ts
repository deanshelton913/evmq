import * as logger from 'loglevel';

/* istanbul ignore next */ // Not worth testing a logger.
export const getLogger = ({silent}) => {
  if (silent) {
    logger.disableAll()
  } else {
    logger.enableAll();
  }
  return logger
};
