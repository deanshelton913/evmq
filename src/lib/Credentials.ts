import os from 'os'
import fs from 'fs'
import path from 'path'
import { RootLogger } from 'loglevel';
import { FailureByDesign } from './FailureByDesign';
import { LoginResponse } from 'evmq-sdk';


export class Credentials {
  public static DIRECTORY = path.join(os.homedir(), '.evmqueue');
  public static FILEPATH =  path.join(Credentials.DIRECTORY, 'user-creds.json');
  public logger: RootLogger;
  public accessToken: string
  public idToken: string
  public refreshToken: string
  public expiresIn: number
  public createdAt: number;
  public secureCookie: string;

  constructor({ 
    accessToken,
    expiresIn,
    refreshToken,
    idToken,
    logger, 
    createdAt
  }:{ 
    accessToken?: string
    expiresIn?: number
    refreshToken?: string
    idToken?: string 
    logger: RootLogger, 
    createdAt?: number
  }){
    this.logger = logger;
    this.accessToken = accessToken;
    this.expiresIn = expiresIn;
    this.createdAt = createdAt || Date.now();
    this.refreshToken = refreshToken;
    this.idToken = idToken;
  }

  public async hydrateFromDisk(){
    const res = await this._readFromDisk();
    this.createdAt = res.createdAt;
    this.accessToken = res.accessToken;
    this.refreshToken = res.refreshToken;
    this.expiresIn = res.expiresIn;
    this.idToken = res.idToken;
  }

  public hydrateFromCognitoResponse({ cognitoResponse }:{ cognitoResponse: LoginResponse}) {
    this.accessToken = cognitoResponse.data.AuthenticationResult.AccessToken;
    this.expiresIn = cognitoResponse.data.AuthenticationResult.ExpiresIn;
    this.refreshToken = cognitoResponse.data.AuthenticationResult.RefreshToken;
    this.idToken = cognitoResponse.data.AuthenticationResult.IdToken;
  }

  public async save() {
    await this._createDirectoryIfNeeded();
    this.logger.info(`Writing to ${Credentials.FILEPATH}`);
    await fs.promises.writeFile(Credentials.FILEPATH, JSON.stringify(this.toJson(), null, 2));
    this.logger.info(`Write complete`);
  }

  public async destroy() {
    const exists = await fs.existsSync(Credentials.FILEPATH);
    if(!exists) {
      this.logger.info('Credentials file does not exist. Nothing to delete.')
      return
    }
    this.logger.info(`Deleting ${Credentials.FILEPATH}`);
    await fs.promises.rm(Credentials.FILEPATH);
    this.logger.info(`Delete complete`);
  }

  public toJson(){
    return {
      accessToken: this.accessToken,
      expiresIn: this.expiresIn,
      refreshToken: this.refreshToken,
      idToken: this.idToken,
      createdAt: this.createdAt,
      secureCookie: this.secureCookie
    };
  }

  private async _createDirectoryIfNeeded() {
    const exists = await fs.existsSync(Credentials.DIRECTORY);
    if(!exists){
      await fs.promises.mkdir(Credentials.DIRECTORY);
    }
  }


  private async _readFromDisk() {
    this.logger.info(`Reading from ${Credentials.FILEPATH}`);
    const exists = await fs.existsSync(Credentials.FILEPATH);
    if(!exists) throw new FailureByDesign('UNAUTHENTICATED','Credentials not found. Please log in.');
    const str = await fs.promises.readFile(Credentials.FILEPATH,'utf-8');
    this.logger.info(`Read complete`);
    return JSON.parse(str);
  }

}


