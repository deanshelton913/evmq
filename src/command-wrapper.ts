import { Credentials } from "./lib/Credentials";
import { Output } from "./lib/Output";
import { getLogger } from "./lib/logger";
import { getMakeRequest, makeRequest as silentMakeRequest } from "./lib/makeRequest";
import { EvmQueueSdk } from "evmq-sdk";
import { RootLogger } from "loglevel";
import { FailureByDesign } from "./lib/FailureByDesign";
import { ApiKeys } from "./lib/ApiKeys";

// This wrapper handles Dependency Injection for all commands.
export async function commandWrapper(program, f: ({ logger, makeRequest, output, credentials, apiKeys, evmQueueSdk }: { 
  output: Output, 
  makeRequest: typeof silentMakeRequest; 
  logger: RootLogger; 
  credentials: Credentials;
  evmQueueSdk: EvmQueueSdk;
  apiKeys: ApiKeys;
}) => Promise<any>
) {
  // Here we add some flags which apply cli-wide.
  program.option("-D, --debug", "Print trace logs to stdout", false);
  program.option("-O, --output-format <format>", "Output as either 'json' or 'table'", 'table');
  program.option('-W, --webserver-url <url>', 'Change the base url of requests to webserver.');
  program.option('-X, --unsafe-log-un-redacted-responses', 'Setting this flag allows local stdout logging of un-redacted HTTP responses which may contain secrets.', false);
  program.parse();

  const { debug, outputFormat, webserverUrl, unsafeLogUnRedactedResponses } = program.opts();

  // Global dependencies injected, and made available into every command.
  const output = new Output({ format: outputFormat });
  const logger = getLogger({ silent: !Boolean(debug) });
  const apiKeys = new ApiKeys({ logger });
  const credentials = new Credentials({ logger });
  const evmQueueSdk = new EvmQueueSdk({ baseURL: webserverUrl || process.env.EVMQ_BASE_URL || 'https://evmqueue.com', logger: logger, unsafeLogUnRedactedResponses });
  const makeRequestWithLogger = getMakeRequest({ logger, baseURL: webserverUrl, unsafeLogUnRedactedResponses });

  try {
    return await f({ evmQueueSdk, logger, makeRequest: makeRequestWithLogger, output, credentials, apiKeys });
  } catch (e) {
    const err = e as FailureByDesign
    // if(e.kind === undefined) console.error(e)
    output.send([{ kind: e.kind||'HTTP_ERROR', message: e.message, ...err.diagnosticInfo }]);
    process.exit(1);
  }
}
