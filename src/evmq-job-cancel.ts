import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('cancel')
  .option('-i --id <id>', 'id of job to cancel')


commandWrapper(program, async ({evmQueueSdk, output, apiKeys})=>{
  const { id } = program.opts();
  await apiKeys.hydrateFromDisk();
  evmQueueSdk.setApiKey({ apiKey: apiKeys.getDefault().key });
  const result = await evmQueueSdk.jobCancel({ id });
  output.send(result.data.data);
});

