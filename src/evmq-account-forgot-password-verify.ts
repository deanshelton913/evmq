import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('forgot-password-verify')
  .option('-e, --email [email]', 'Email address.')
  .option('-p, --password [password]', 'Password.')
  .option('-c, --confirmationCode <confirmationCode>', 'ConfirmationCode code found in email.')

commandWrapper(program, async ({ evmQueueSdk, output })=>{
  const { email, confirmationCode, password } = program.opts()
  if(!email) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --email"')
  if(!password) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-p, --password"')
  if(!confirmationCode) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-c, --confirmationCode"')
  const res = await evmQueueSdk.forgotPasswordVerify({email, password, confirmationCode})
  output.send(res.data);
})