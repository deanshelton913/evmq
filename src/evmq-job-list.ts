import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('list')
  .option('-p --page-size [size]', 'Number of items to return (1-100)','10')
  .option('-s --job-status [jobStatus]', 'Filter by a single job status (RUNNING, DONE, CANCELLED)', null)
  .option('-n --page-number [number]', 'Page number to return. (1 based)','1')

commandWrapper(program, async ({evmQueueSdk, output, apiKeys})=>{
  await apiKeys.hydrateFromDisk();
  evmQueueSdk.setApiKey({ apiKey: apiKeys.getDefault().key });
  const { pageSize, pageNumber, jobStatus } = program.opts();
  const result = await evmQueueSdk.jobList({ pageSize, pageNumber, jobStatus });
  output.send(result.data.data.map((x,i)=>({result:i+1, ID:x.ID, CreatedAt:x.CreatedAt, status: x.status, logsFound: x.logsFound})))
});

