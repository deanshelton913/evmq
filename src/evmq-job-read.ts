import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('read')
  .option('-i --id <id>', 'Job ID')
  .option('-n --number-of-items <number>', 'Number of items to pop off of the queue')


commandWrapper(program, async ({ evmQueueSdk, output, apiKeys })=>{
  await apiKeys.hydrateFromDisk();
  evmQueueSdk.setApiKey({ apiKey: apiKeys.getDefault().key });
  const { id, numberOfItems } = program.opts();
  const result = await evmQueueSdk.jobRead({ id, take: numberOfItems });
  output.send(result.data.data)
});

