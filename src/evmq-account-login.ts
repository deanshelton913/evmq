import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();

program
  .name('login')
  .option('-e, --email [email]', 'Email address. A email will be sent to verify.')
  .option('-p, --password [password]', 'Password. You will use this password to login later.')

commandWrapper(program, async ({ output, credentials, evmQueueSdk })=>{
  const { email, password } = program.opts();
  if(!email) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --email"')
  if(!password) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-p, --password"')
  const res = await evmQueueSdk.login({email, password})
  if (res.status === 400) {
    throw new FailureByDesign('BAD_REQUEST', res.data?.error)
  }
  credentials.accessToken = res.data.data.AuthenticationResult.AccessToken;
  credentials.expiresIn = res.data.data.AuthenticationResult.ExpiresIn;
  credentials.refreshToken = res.data.data.AuthenticationResult.RefreshToken;
  credentials.idToken = res.data.data.AuthenticationResult.IdToken;
  credentials.secureCookie = res.headers['set-cookie'][0];
  await credentials.save();
  
  output.send({ loggedIn: true });
})