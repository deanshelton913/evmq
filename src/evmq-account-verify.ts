import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('verify')
  .option('-e, --email [email]', 'Email address. A email will be sent to verify.')
  .option('-c, --confirmationCode <confirmationCode>', 'ConfirmationCode code found in registration email.')

commandWrapper(program, async ({ evmQueueSdk, output })=>{
  const { email, confirmationCode } = program.opts()
  if(!email) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-e --email"')
  if(!confirmationCode) throw new FailureByDesign("PARAM_ERROR", 'Missing code: "-c, --code"')
  const res = await evmQueueSdk.verify({email, confirmationCode})
  if(res.data.error) {
    output.send(res.data);
  } else {
    output.send({
      verified: res.data, 
    })
  }
})