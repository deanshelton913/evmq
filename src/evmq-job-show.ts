import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('show')
  .option('-i --id <id>', 'Job ID')


commandWrapper(program, async ({ evmQueueSdk, output, apiKeys })=>{
  await apiKeys.hydrateFromDisk();
  evmQueueSdk.setApiKey({ apiKey: apiKeys.getDefault().key });
  const { id } = program.opts();
  const result = await evmQueueSdk.jobShow({ id });
  if(output.format === 'table') {
    const tableFormat = {
      ...result.data.data.job, 
      approximateQueueLength:result.data.data.approximateQueueLength,
      workerStatus: JSON.stringify(result.data.data.workerStatus)
    }
    output.send(tableFormat)
  } else {
    output.send(result.data.data)
  }

});

