import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';
import { FailureByDesign } from './lib/FailureByDesign';

const program = new Command();
program
  .name('add')
  .option('-i --id <id>', "Api Key Id")
  .option('-k --key <key>', "The api key itself")

commandWrapper(program, async ({ output, apiKeys })=>{
  const { id, key } = program.opts();  
  if(!id) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-i --id"')
  if(!key) throw new FailureByDesign("PARAM_ERROR", 'Missing Required Parameter: "-k, --key"')
  apiKeys.add({ id, key })
  await apiKeys.save();
  output.send(apiKeys.asOutput());
})