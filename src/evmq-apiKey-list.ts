import { Command } from 'commander';
import { commandWrapper } from './command-wrapper';

const program = new Command();
program
  .name('list')
  .option('-l --local','list only local keys. i.e.: read from disk.')

commandWrapper(program, async ({evmQueueSdk, output, apiKeys, credentials})=>{
  const { local } = program.opts();
  if(local) {
    await apiKeys.hydrateFromDisk();
    output.send(apiKeys.asOutput());
  } else {
    await credentials.hydrateFromDisk();
    evmQueueSdk.setIdToken({ idToken: credentials.idToken });
    const res = await evmQueueSdk.apiKeyList();
    output.send(res.data.data.apiKeys);
  }
})